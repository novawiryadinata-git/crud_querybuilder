@extends('landing.master.app')

@section('title', 'CRUD-Mahasiswa')
@section('content')
<section>
    <div class="conten">
        <h2>Registrasi</h2>
        <div class="newdata">
            <h3>Edit</h3>
            @foreach ($data_mhs as $data)
            <form action="/update" method="POST">
                {{csrf_field()}}
                <table class="tablenewdata">
                    <input type="hidden" name="id" value="{{ $data->id }}"> <br />
                    <tr>
                        <td>NAME</td>
                        <td> <input type="text" name="nama_mahasiswa" value="{{ $data->nama_mahasiswa }}"  required="required"  class="form-control">
                        </td>
                    </tr>
                    <tr>
                        <td>NIM</td>
                        <td> <input type="text" name="nim_mahasiswa"  value="{{ $data->nim_mahasiswa }}" required="required"  class="form-control">
                        </td>
                    </tr>
                    <tr>
                        <td>KELAS</td>
                        <td> <input type="text" name="kelas_mahasiswa"  value="{{ $data->kelas_mahasiswa }}" required="required"  class="form-control">
                        </td>
                    </tr>
                    <tr>
                        <td>PRODI</td>
                        <td> <input type="text" name="prodi_mahasiswa"  value="{{ $data->prodi_mahasiswa }}" required="required" class="form-control">
                        </td>
                    </tr>
                    <tr>
                        <td>FAKULTAS</td>
                        <td> <input type="text" name="fakultas_mahasiswa"  value="{{ $data->fakultas_mahasiswa }}" required="required"  class="form-control">
                        </td>
                    </tr>
                </table>
                <input type="submit" value="SAVE" class="save">
                <!-- input type submit yang nantinya akan mengarah ke kondisi isset POST 'Save' -->
            </form>
            @endforeach
        </div>
    </div>
</section>






@endsection
