<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class RegisterController extends Controller
{
    // Register Controller
    public function register(){
        $hasil = DB::table('mahasiswa')->get();
        return view('/registrasi',['data_mhs'=>$hasil]);
    }

    public function save(Request $req){
        $nama = $req->nama_mahasiswa;
        $nim = $req->nim_mahasiswa;
        $kelas = $req->kelas_mahasiswa;
        $prodi = $req->prodi_mahasiswa;
        $fakultas = $req->fakultas_mahasiswa;

        DB::table('mahasiswa')->insert(
            [
                'nama_mahasiswa'=>$nama,
                'nim_mahasiswa'=>$nim,
                'kelas_mahasiswa'=>$kelas,
                'prodi_mahasiswa'=>$prodi,
                'fakultas_mahasiswa'=>$fakultas,
            ]
        );
        return redirect('/registrasi');
    }

    public function delete($id){
        DB::table('mahasiswa')->where('id', $id)->delete();

        return redirect('/registrasi');
    }

    public function edit($id)
    {
        // mengambil data mahasiswa berdasarkan id yang dipilih
        $data_mhs = DB::table('mahasiswa')->where('id', $id)->get();
        // passing data mahasiswa yang didapat ke view edit.blade.php
        return view('/edit', ['data_mhs' => $data_mhs]);
    }

    public function update(Request $req)
    {
        // update data mahasiswa berdasarkan id
        DB::table('mahasiswa')->where('id', $req->id)->update([
            'nama_mahasiswa' => $req->nama_mahasiswa,
            'nim_mahasiswa' => $req->nim_mahasiswa,
            'kelas_mahasiswa' => $req->kelas_mahasiswa,
            'prodi_mahasiswa' => $req->prodi_mahasiswa,
            'fakultas_mahasiswa' => $req->fakultas_mahasiswa,
        ]);
        // alihkan halaman ke halaman home
        return redirect('/registrasi');
    }
}
