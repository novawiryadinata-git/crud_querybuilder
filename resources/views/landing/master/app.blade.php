<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>@yield('title')</title>
    @include('landing.master._headerInclude')
</head>
<body>
    <div class="container">
    {{-- header --}}
        <div class="header">
            <ul>
                <li>
                    <div class="inilogo"></div>
                </li>
                <li>
                    <h1 class="judul">TITIK2V</h1>
                </li>
            </ul>
            <div class="calendar"></div>
            </li>
            <!-- menampilkan format pewaktu WITA -->
            <div class="date">
                <?php
                date_default_timezone_set('Asia/Ujung_Pandang');
                echo date('l, d-m-Y  h:i:s a');
                ?>
            </div>
        </div>
                @yield('content')
         <!-- footer -->
         <div class="footer">
            <p class="copy">Copyright 2021, TITIK2V</p>
        </div>
    </div>
</body>
</html>
