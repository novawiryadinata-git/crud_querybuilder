<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/registrasi', [App\Http\Controllers\RegisterController::class,'register']);

Route::post('/simpan',[\App\Http\Controllers\RegisterController::class,'save']);
Route::get('/hapus/{id}',[\App\Http\Controllers\RegisterController::class,'delete']);

Route::get('/edit/{id}',[\App\Http\Controllers\RegisterController::class,'edit']);
Route::post('/update',[\App\Http\Controllers\RegisterController::class,'update']);
